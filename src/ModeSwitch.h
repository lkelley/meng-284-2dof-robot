#include <Arduino.h>

class ModeSwitch {
    private:
        unsigned long debounceStart = 0;
        unsigned long debounceEnd = 0;
        int pin;
        int state;
        
        const int DEBOUNCE_TIME = 20;

        int tempState = LOW;
        int debounceState = LOW;
        bool changed = false;
    
    public:
        ModeSwitch();
        void attach(int pin);
        int getMode();
};