#include <Arduino.h>
#include <math.h>
#include "LinearServo.h"
#include "ModeSwitch.h"
#include <ArduinoSTL.h>
#include <algorithm>
#include <queue>
#include "Screen.h"
#include <stdio.h>
#include <MemoryFree.h>


/**
 * I/O
 */
const int MODE_MANUAL = LOW; // State for manual mode
const int MODE_AUTO = HIGH; // State for automatic mode

const int X_POT = A0; // X axis pot input pin
const int Y_POT = A1; // Y axis pot input pin
const int MODE_PIN = 10; // Mode DIP switch pin
const int SHOULDER_PIN = 9;
const int ELBOW_PIN = 8;

// Screen object
//Screen screen(0x27, 20, 4);

/**
 * Calibration Data
 */
const int X_MIN = 75; // Min X coordinate [mm]
const int X_MAX = 150; // Max X coordinate [mm]

const int Y_MIN = 75; // Min Y coordinate [mm]
const int Y_MAX = 150; // Max Y coordinate [mm]

const int length1 = 130; // Length of first link [mm]
const int length2 = 125; // Length of second link [mm]

const int POT_DEADBAND = 8; // Required change in pot inputs [DAC]

const int MIN_MOVEMENT = 4; // Minimum axis movement [mm]


/**
 * Auto Mode Variables
 */
const int autoPointCount = 5;
const int autoPoints[autoPointCount][2] = {
  {75,75},
  {75,150},
  {150,75},
  {100,150},
  {150,150}
};
int autoLoopCounter = 0;


/**
 * Function forward declarations
 */
double getShoulderAngle(int x, int y);
double getElbowAngle(int x, int y, double shoulderAngle);
double getXPosition(double shoulderDegrees, double elbowDegrees);
double getYPosition(double shoulderDegrees, double elbowDegrees);
void calculatePathPoints(double start, double end, int numPoints, std::queue<double> &points);


/**
 * Servo objects
 */
LinearServo shoulder;
LinearServo elbow;

// Mode switch controller
ModeSwitch modeSwitch;

/**
 * Placeholders and flags
 */
int x_pot, x_pot_old; // Placeholder for x pot read
int y_pot, y_pot_old; // Placeholder for y pot read
double x; // Current x set point (pot reading)
double y; // Current y set point (pot reading)
double xNew; // Holder for new x set point
double yNew; // Holder for new y set point
bool positionChange = false; // Change flag for position
int mode = -1; // Mode storage


/**
 * Initial setup function run at program start
 */
void setup() {
  Serial.begin(9600);

  // Set potentiometer inputs
  pinMode(X_POT, INPUT);
  pinMode(Y_POT, INPUT);

  // Attach mode switch to pin (function handles pinMode)
  modeSwitch.attach(MODE_PIN);

  // Attach joint pins
  shoulder.attach(SHOULDER_PIN);
  shoulder.setOffsetAngle(104);
  shoulder.setCalibrationCurve(0.0003, 0.8608, 3.6196);

  elbow.attach(ELBOW_PIN);
  elbow.setOffsetAngle(180);
  elbow.setCalibrationCurve(0.0002, 0.8592, 2.7023);
  Serial.println("MONITORING BEGUN");
  delay(3000);
}


/**
 * Main Program Loop
 */
void loop() {
  // Main state switch
  switch (modeSwitch.getMode()) {
    case MODE_AUTO: {
      if (mode != MODE_AUTO) { // Mode has been changed
        Serial.println("Auto Mode");
        autoLoopCounter = 0; // Reset auto loop
        mode = MODE_AUTO;
      }


      // Move to next point if current movement is completed
      if (shoulder.getMovementComplete() && elbow.getMovementComplete()) {
        Serial.println("Setting next target");
        // Set next points
        xNew = autoPoints[autoLoopCounter][0];
        yNew = autoPoints[autoLoopCounter][1];
        positionChange = true;

        autoLoopCounter++;
        if (autoLoopCounter >= autoPointCount) autoLoopCounter = 0; // Reset loop counter
      }

      break;
    }
    
    /*
    Potentiometer reads are converted to X,Y coordinates. As this
    conversion uses the map() function, coordinates are integers.
    */
    case MODE_MANUAL: {
      if (mode != MODE_MANUAL) { // Mode has changed
        Serial.println("Manual Mode");
        mode = MODE_MANUAL;
      }

      // Read X pot and check for changes
      x_pot = analogRead(X_POT);

      if (abs(x_pot - x_pot_old) > POT_DEADBAND) {
        x_pot_old = x_pot;
        positionChange = true;
        xNew = map(x_pot, 0, 1023, X_MIN, X_MAX);
      }

      // Read Y pot and check for changes
      y_pot = analogRead(Y_POT);

      if (abs(y_pot - y_pot_old) > POT_DEADBAND) {
        y_pot_old = y_pot;
        positionChange = true;
        yNew = map(y_pot, 0, 1023, Y_MIN, Y_MAX);
      }

      break;
    }
  }


  // Calculate positions and set servos if position has changed
  if (positionChange) {
    Serial.print("New position found: ");
    Serial.print(xNew);
    Serial.print(", ");
    Serial.println(yNew);

    // Reset change flag
    positionChange = false;

    // Stop the servos
    shoulder.stop();
    elbow.stop();

    // Update screen
    //screen.updatePosition((uint8_t)xNew, (uint8_t)yNew);

    // Get the destination angles for calculating total movement time
    double shoulderAngle = getShoulderAngle(xNew, yNew);
    double elbowAngle = getElbowAngle(xNew, yNew, shoulderAngle);


    /*
    Calculate sync time
    */
    double syncTime;

    // Determine current location
    double currentShoulderAngle = shoulder.getUnadjustedSetPoint();
    double currentElbowAngle = elbow.getUnadjustedSetPoint();

    // Determine the max movement speed
    int maxDegreesPerSecond = std::min(shoulder.getMaxDegreesPerSecond(), elbow.getMaxDegreesPerSecond());
    Serial.println(maxDegreesPerSecond);

    // Determine the shortest axis move in mm, divide by minimum movement to get point count
    int numPoints = std::min(abs(xNew - x), abs(yNew - y)) / MIN_MOVEMENT;
    numPoints = std::max(numPoints, 1);

    // Calculate path points
    std::queue<double> xPoints;
    std::queue<double> yPoints;
    calculatePathPoints(x, xNew, numPoints, xPoints);
    calculatePathPoints(y, yNew, numPoints, yPoints);
    

    // Calculate the movement time in seconds
    syncTime = std::max(
      abs(shoulderAngle - currentShoulderAngle),
      abs(elbowAngle - currentElbowAngle)
    ) / maxDegreesPerSecond;

    std::queue<double> shoulderAngles;
    std::queue<double> elbowAngles;

    // Calculate the joint angle for each point
    while (!xPoints.empty()) {
      shoulderAngles.push(getShoulderAngle(xPoints.front(), yPoints.front()));
      elbowAngles.push(getElbowAngle(xPoints.front(), yPoints.front(), shoulderAngles.back()));
      xPoints.pop();
      yPoints.pop();
    }

    // Push the paths
    shoulder.setPath(shoulderAngles, syncTime);
    elbow.setPath(elbowAngles, syncTime);
    Serial.println("Paths set");
    delay(2000);

    // Store new points
    x = xNew;
    y = yNew;
  }

  // Loop functions
  shoulder.update();
  elbow.update();
}


/**
 * Returns the shoulder angle based on and x,y coordinate
 * 
 * Returns degrees
 */
double getShoulderAngle(int x, int y) {
  double c2 = (pow(x, 2) + pow(y, 2) - pow(length1, 2) - pow(length2, 2)) / (double)(2 * length1 * length2);

  return degrees(atan2(
    ((-1 * length2 * sqrt(1 - pow(c2, 2))) * x + (length1 + length2 * c2) * y),
    ((length1 + length2 * c2) * x + (length2 * sqrt(1 - pow(c2, 2))) * y)
  ));
}


/**
 * Returns the elbow angle based on the end effector x,y coordinate and the shoulder angle (in degrees)
 * 
 * Returns degrees
 */
double getElbowAngle(int x, int y, double shoulderAngle) {
  return degrees(atan2(
    (double)y - length1 * sin(radians(shoulderAngle)),
    (double)x - length1 * cos(radians(shoulderAngle))
  )) - shoulderAngle;
}


/**
 * Returns the end effector x coordinate from the shoulder and elbow angles
 * 
 * Returns mm
 */
double getXPosition(double shoulderDegrees, double elbowDegrees) {
  return length1 * cos(radians(shoulderDegrees)) + length2 * cos(radians(elbowDegrees));
}


/**
 * Returns the end effector y coordinate from the shoulder and elbow angles
 * 
 * Returns mm
 */
double getYPosition(double shoulderDegrees, double elbowDegrees) {
  return length1 * sin(radians(shoulderDegrees)) + length2 * sin(radians(elbowDegrees));
}


/**
 * Calculates path intervals from a start to an end point and stores them in the passed queue
 */
void calculatePathPoints(double start, double end, int numPoints, std::queue<double> &points) {
  // Create points holder
  std::queue<double> newPoints;

  // Interpolate all but the last point
  for (int progress = 1; progress < numPoints; progress++) {
    // Calculate step and add to queue
    newPoints.push(progress * (end - start) / numPoints + start);
  }

  // Account for floating point errors by ensuring the last point is accurate
  newPoints.push(end);

  // Put new points in the points queue
  std::swap(points, newPoints);
}
