#ifndef Path_h
#define Path_h

#include "Point.h"
#include <Arduino.h>

class Path {
    private:
        Point *first = NULL;
        Point *last = NULL;

        const int movement = 1;
    
    public:
        Point* getNext();

        void add(Point *point) {
            
            this->last->setNext(point);
        }
};

#endif
