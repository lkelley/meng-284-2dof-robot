#include "LinearServo.h"
#include <Arduino.h>
#include <ArduinoSTL.h>
#include <queue>

LinearServo::LinearServo() {}

void LinearServo::attach(int pin) {
    servo.attach(pin, this->US_MIN, this->US_MAX);
}

/**
 * degrees - Target position in degrees
 * syncTime - Duration of move in seconds
 */
void LinearServo::goTo(double degrees, double duration) {
    // Store unadjusted set point
    this->unadjustedSetPoint = this->getDegreesAsUS(degrees);

    // Get set point adjusted for offset and servo response
    degrees = this->getAdjustedSetPoint(degrees);
    this->setPoint = this->getDegreesAsUS(degrees);

    // Set timing vars
    this->startTime = micros();
    this->endTime = this->startTime + (long)(duration * 1000000);

    // Set target final position for move
    this->startPoint = this->position;
}

void LinearServo::setPath(std::queue<double> path, double syncTime) {
    this->movementComplete = false;
    this->points = path;
    this->syncTime = syncTime / path.size();
}

void LinearServo::setOffsetAngle(int degrees) {
    this->axisOffsetAngle = degrees;
}

void LinearServo::stop() {
    // Clear the points queue
    std::queue<double> empty;
    std::swap(this->points, empty);

    // Invalidate timer
    this->endTime = 0;
}

void LinearServo::update() {
    // Grab current time
    this->currentTime = micros();

    // Interpolate target position if there is time remaining in the move
    if (this->currentTime < this->endTime) { // Timing is not protected against overflow
        this->position = (this->currentTime - this->startTime) * (this->setPoint - this->startPoint)
            / (this->endTime - this->startTime);
    }
    // No time left in the move, rapid to final position (should be there by this point anyway)
    else {
        this->position = this->setPoint;

        // Set the next point, if there is one
        if (!this->points.empty()) {
            // Go to next point
            this->goTo(this->points.front(), this->syncTime);

            // Remove the point from the queue
            this->points.pop();
        }
        // No remaining points, movement complete
        else if (!this->movementComplete) {
            this->movementComplete = true;
        }
    }
    
    // Set servo position
    servo.writeMicroseconds(this->position);
}

double LinearServo::getPosition() {
    return this->getUSAsDegrees(this->position);
}

void LinearServo::setCalibrationCurve(double A, double B, double C) {
    this->A = A;
    this->B = B;
    this->C = C;
}

int LinearServo::getDegreesAsUS(double degrees) {
    return (degrees - this->DEGREES_MIN) * (this->US_MAX - this->US_MIN) 
        / (this->DEGREES_MAX - this->DEGREES_MIN) + this->US_MIN;
}

double LinearServo::getUSAsDegrees(int us) {
    return (us - this->US_MIN) * (this->DEGREES_MAX - this->DEGREES_MIN) 
        / (this->US_MAX - this->US_MIN) + this->DEGREES_MIN;
}

int LinearServo::getMaxDegreesPerSecond() {
    return this->MAX_DEGREES_PER_SECOND;
}

bool LinearServo::getMovementComplete() {
    return this->movementComplete;
}