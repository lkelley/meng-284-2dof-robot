#include <Servo.h>
#include <math.h>
#include <ArduinoSTL.h>
#include <queue>

class LinearServo
{
    private:
        Servo servo;
        unsigned long startTime; // Start time in microseconds
        unsigned long endTime; // End time in microseconds
        int setPoint; // Set point in microseconds
        int unadjustedSetPoint; // Unadjusted set point in degrees
        int position; // Current position in microseconds
        int startPoint; // Start point of movement in microseconds
        double syncTime;
        int axisOffsetAngle = 0; // Reference axis offset in degrees, servo 0 CCW from reference 0
        bool movementComplete = true;

        unsigned long currentTime; // Placeholder for current time in calculations
        int currentPosition; // Placeholder for current position in calculations

        const int US_MIN = 900;
        const int US_MAX = 2100;
        const int DEGREES_MIN = 0;
        const int DEGREES_MAX = 180;

        const int MAX_DEGREES_PER_SECOND = 60;

        /*
        Calibration Curve
        setPoint(degrees) = Ax^2 + Bx + C
        */
        double A = 0, B = 1, C = 0;

        std::queue<double> points;

        double getOffsetSetPoint(double requested) {
            return this->axisOffsetAngle - requested;
        }

        double getAdjustedSetPoint(double requested) {
            requested = this->getOffsetSetPoint(requested);
            return this->A * pow(requested, 2) + B * requested + C;
        }

    public:
        LinearServo();
        void attach(int pin);
        void goTo(double degrees, double duration);
        void setPath(std::queue<double> path, double syncTime);
        void setOffsetAngle(int degrees);
        void stop();
        void update();
        bool getMovementComplete();

        double getPosition();
        double getUnadjustedSetPoint() {
            return this->unadjustedSetPoint;
        }

        void setCalibrationCurve(double A, double B, double C);

        int getDegreesAsUS(double degrees);
        double getUSAsDegrees(int us);

        int getMaxDegreesPerSecond();
};
