#ifndef Point_h
#define Point_h

class Point {
    private:
        int x;
        int y;
        Point *next;
    
    public:
        Point(int x, int y);
        
        Point* getNext() {
            return this->next;
        }

        void setNext(Point *point) {
            this->next = point;
        }

        int getX() {
            return this->x;
        }

        int getY() {
            return this->y;
        }
};

#endif
