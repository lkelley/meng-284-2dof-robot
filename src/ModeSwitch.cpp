#include "ModeSwitch.h"
#include <Arduino.h>

ModeSwitch::ModeSwitch() {}

void ModeSwitch::attach(int pin) {
    pinMode(pin, INPUT);
    this->pin = pin;
}

int ModeSwitch::getMode() {
    // Get current pin state
    this->debounceState = digitalRead(this->pin);

    if (this->debounceState != this->state) { // Change detected
        // Current state has not been debounced
        if (this->tempState != this->debounceState) {
            // Start debounce timer and store read state
            this->debounceStart = millis();
            this->debounceEnd = this->debounceStart + this->DEBOUNCE_TIME;
            this->tempState = this->debounceState;
        }
        // Store the new state once the timer has elapsed
        else if (millis() >= this->debounceEnd) {
            this->state = this->tempState;
        }
    }

    return this->state;
}