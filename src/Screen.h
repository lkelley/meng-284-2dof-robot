#include <ArduinoSTL.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

class Screen {
    private:
        LiquidCrystal_I2C lcd;
        uint8_t columns = 0;
        uint8_t rows = 0;
        uint8_t xPrintoutCoordinate[2] = {3, 3}; // x,y coordinate of X position display
        uint8_t yPrintoutCoordinate[2] = {10, 3}; // x,y coordinate of Y position display

    public:
        Screen(uint8_t address, uint8_t columns, uint8_t rows) : lcd(address, columns, rows) {
            this->columns = columns;
            this->rows = rows;

            // Initialize LCD
            lcd.init();
            lcd.backlight();
            lcd.clear();

            // Print template
            std::string displayTemplate[this->rows];
            displayTemplate[0] = std::string("*", this->columns);
            displayTemplate[1] = "2-DOF Robot Control";
            displayTemplate[2] = std::string("*", this->columns);
            displayTemplate[3] = "X:     Y:";
            String string;

            lcd.home();
            lcd.print(displayTemplate[0].c_str());
        }

        // Update displayed end effector coordinate
        void updatePosition(int x, int y) {
            lcd.setCursor(this->xPrintoutCoordinate[1], this->xPrintoutCoordinate[0]);
            lcd.print(x);

            lcd.setCursor(this->yPrintoutCoordinate[1], this->yPrintoutCoordinate[0]);
            lcd.print(y);
        }
};